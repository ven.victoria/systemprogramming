#include <windows.h>
#include <iostream>
#include <conio.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	HANDLE hFileMapping;
	int* buf = NULL;
	LPCSTR name = "data.dat";
	HANDLE hmutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, TEXT("Mutex"));
	WaitForSingleObject(hmutex, INFINITE);
	hFileMapping = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, TEXT("data"));
	if (hFileMapping != NULL)
	{
		printf("fileMappingCreate - OpenFileMapping approved, fname = %s\n", name);
		buf = (int*)MapViewOfFile(hFileMapping, FILE_MAP_WRITE, NULL, NULL, NULL);
	}
	printf("������� ����������:\n");
	for (int i = 0; i < 25 - 1; i++) {
				for (int j = 0; j < 25 - i - 1; j++) {
					__try {
						Sleep(500);
						if (buf[j] > buf[j + 1]) {
							int temp = buf[j];
							buf[j] = buf[j + 1];
							buf[j + 1] = temp;
						}
					}
						__finally {
							ReleaseMutex(hmutex);
						}
					}
				}
	printf("³���������� �����:\n");
	for (int i = 0; i < 25; i++) {
		printf("%d ", buf[i]);
	}
	printf("\n");
	printf("��������� ����-��� ������...\n");
	getchar();
	UnmapViewOfFile(buf);
	CloseHandle(hFileMapping);
	return 0;
}


