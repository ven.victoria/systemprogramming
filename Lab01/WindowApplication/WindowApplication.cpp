#include <stdio.h>
#include <windows.h>
TCHAR Stars[] = TEXT("********************************************************************************************************");

HANDLE hFileMapping;
HANDLE hmutex;
int* mapOfFile;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	PSTR szCmdLine, int iCmdShow)
{
	//static char szAppName[] = "HelloWin";
	TCHAR szAppName[] = TEXT("HelloWin");
	HWND        hwnd;
	MSG         msg;
	WNDCLASSEX  wndclass;

	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc = WndProc;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName = NULL;
	wndclass.lpszClassName = szAppName;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName,         // window class name
		TEXT("Lab 1"),     // window caption
		WS_OVERLAPPEDWINDOW,     // window style
		CW_USEDEFAULT,           // initial x position
		CW_USEDEFAULT,           // initial y position
		CW_USEDEFAULT,           // initial x size
		CW_USEDEFAULT,           // initial y size
		NULL,                    // parent window handle
		NULL,                    // window menu handle
		hInstance,               // program instance handle
		NULL);		             // creation parameters

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HDC         hdc;
	PAINTSTRUCT ps;
	RECT        rect;

	switch (iMsg)
	{
	case WM_CREATE:
		hmutex = NULL;
		hmutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, TEXT("Mutex"));
		if (hmutex == NULL) {
			PostQuitMessage(1);
		}
		WaitForSingleObject(hmutex, INFINITE);

		hFileMapping = OpenFileMapping(FILE_MAP_WRITE, FALSE, TEXT("data"));
		if (hFileMapping == NULL)
		{
			PostQuitMessage(1);
		}

		mapOfFile = (int*)MapViewOfFile(hFileMapping, FILE_MAP_ALL_ACCESS, NULL, NULL, NULL);

		SetTimer(hwnd, 0, 800, NULL);

		ReleaseMutex(hmutex);

		return 0;

	case WM_TIMER:
		InvalidateRect(hwnd, NULL, TRUE);
		return 0;

	case WM_PAINT:
		hdc = BeginPaint(hwnd, &ps);

		GetClientRect(hwnd, &rect);

	

		char mas[10];
		int number;
		__try {
			WaitForSingleObject(hmutex, INFINITE);
			for (int i = 0; i < 25; i++)
			{
				number = mapOfFile[i];
				TextOut(hdc, 10, i * 25, Stars, number);//����������, ���������� ��������� ������� �,�, ������ ��������, �����
				//Stars 
			}
		}
		__finally {
			ReleaseMutex(hmutex);
		}

		EndPaint(hwnd, &ps);
		return 0;

	case WM_DESTROY:
		KillTimer(hwnd, 0);
		UnmapViewOfFile(mapOfFile);
		CloseHandle(hFileMapping);
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}
