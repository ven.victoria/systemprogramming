#include <windows.h>
#include <iostream>
#include <conio.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	HANDLE hFileMapping;
	int* buf = NULL;
	LPCSTR name = "data.dat";
	HANDLE hmutex = OpenMutex(MUTEX_ALL_ACCESS, FALSE, TEXT("Mutex"));
	
	hFileMapping = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, TEXT("data"));
	if (hFileMapping != NULL)
	{
		printf("fileMappingCreate - OpenFileMapping approved, fname = %s\n", name);
		buf = (int*)MapViewOfFile(hFileMapping, FILE_MAP_WRITE, NULL, NULL, NULL);
	}
	printf("������� ����������:\n");
	int key = 0, j = 0;
	for (int i = 1; i < 25; i++) {
		key = buf[i];
		j = i - 1;
		while (j >= 0 && buf[j] < key) {
			Sleep(500);
			__try {
				WaitForSingleObject(hmutex, INFINITE);
				buf[j + 1] = buf[j];
				j--;
			}
			__finally {
				ReleaseMutex(hmutex);
			}
		}
		buf[j + 1] = key;
	}
	printf("³���������� �����:\n");
	for (int i = 0; i < 25; i++)
	{
		printf("%d ", buf[i]);
	}
	printf("\n");
	printf("��������� ����-��� ������...\n");
	getchar();
	UnmapViewOfFile(buf);
	CloseHandle(hFileMapping);
	return 0;
}
